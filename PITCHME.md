# Scrum Intro
> Es ist gut einen Plan zu haben.
> Besser ist es wenn der Plan auch angepasst werden kann. 

+++

## Scrum ist ein Rahmenwerk
* [Scrum Guide](http://www.scrumguides.org/scrum-guide.html) 
* Fix: Klare Regeln und Strukturen 
* Frei: Techniken und Werkzeuge

+++

## Wo wird Scrum eingesetzt?
> Im komplexen Umfeld, bei dynamischen Anforderungen.

* Auf Änderungen reagieren können
* Negativ Beispiel Wasserfall Prozess

+++

## Empirischer Prozess 
> Aus Erfahrungen lernen und Entscheidungen auf dieser Erfahrung treffen.

* Inkrementel
* Iteration

* Risiken mimimieren 
* Prognosen optimieren 

---

## Agiles Manifest
* Basis [Agile Manifet](http://agilemanifesto.org/). 
* Grundwerte
* Prinzipien

+++

## Grundwerte
Grundwerte | noch wichtigere Grundwerte 
------------ | ------------
Prozesse und Werkzeuge | Individuen und Interaktionen 
Umfassende Dokumentation | Funktionierende Software
Vertragsverhandlung | Zusammenarbeit mit dem Kunden
Befolgen eines Plans | Reagieren auf Veränderung

---

## 3 Säulen
* Transparenz (Transparency)
* Überprüfung (Inspection)
* Anpassung (Adaptation)

---

## Scrum Werte
* Fokus
* Mut
* Offenheit
* Selbstverpflichtung
* Respekt

---

## Scrum Team

* 1 Product Owner
* 1 Scrum Master
* 3-9 Entwickler (Entwickler = Tester!)

![Scrum Team](https://openclipart.org/download/272476/working-team.svg)

+++

## Product Owner
> Ist verantwortlich für das zu entwickelnde Produkt und den 
> Return On Investment.

* Kundenkontakt
* Mehrwert maximieren
* PBIs erklären
* Product Backlog priorisieren
* Erarbeitet PBIs mit Entwicklungsteam
* Entscheidet ob Inkrement ausgeliefert wird

+++

## Scrum Master
> Optimiert die Teamleistung.

* Scrum Werte vermitteln
* Räumt Hindernisse aus dem Weg
* Ereignisse finden statt
* Entwickler und PO unterstützen
* Kommunikation fördern
* Gute Arbeitsumgebung

+++

## Entwicklungsteam
> Arbeitet selbstorganisiert und liefert das Produkt. 

* Tagesplanung am Daily
* PBIs nach DoD umsetzen
* Kostengünstige Lösungen
* Arbeitsweise optimieren
* Arbeitsanweisungen nur von PO annehmen
* Sprintbacklog prognostizieren & umsetzen

---

## Ereignisse
* Sprint
* Sprint Planning
* Daily Scrum
* Sprint Review
* Sprint Retrospektive

+++

## Sprint
> Während eines Sprints werden PBIs vom Entwicklungsteam in ein Inkrement umgesetzt.

* 2-4 Wochen
* Keine Unterbrechung
* Sprint Ziel
* Kann nur von PO abgebrochen werden
* Plan, Do, Check, Act Phasen

+++

## Sprint Planning
> Im Sprint Planning wird die Arbeit für den kommenden Sprint geplant.

Planning 1 (was): 
* Sprint Ziel & prognostizierter Sprint PBIs 

Planning 2 (wie): 
* Sprint Backlog

+++

## Daily Scrum
> Das Entwicklungsteam synchronisiert sich und plant anstehende Arbeit.

Plan, Do, Check, Act Phasen:
* Was wurde seit der letzten Besprechung erreicht?
* Was wird in den nächsten 24h erledigt?
* Welche Hindernisse gibt es?

> Nur Entwickler bringen sich aktiv ein.

+++

## Sprint Review
> Vorstellung des entwickelten Inkrements durch Entwickler an Stakeholder.

* reales Produkt (keine Folien)
* Herausforderungen und Lösungen
* Feedback einholen
* PO aktualisiert Product Backlog
* Status Release Candidate (RC) 

+++

## Sprint Retrospektive
> Sprint wird reflektiert. Was möchte das Team im nächsten Sprint anders machen.

* Was lief gut?
* Probleme und Herausforderungen
* Was können wir optimieren?

> Massnahmen

---

## Artefakte
* Product Backlog
* Sprint Backlog
* Inkrement

* Sprint Ziel
* Definition of Done

+++

## Product Backlog
> Das Product Backlog ist eine priorisierte Auflistung von Allem, was für das Produkt benötigt wird.

* Nur ein Product Backlog
* PO priorisiert
* Hochpriorisierte PBIs (Definition of Ready)
* PBIs sind dynamisch
* Jeder kann PBIs einbringen

+++

## Sprint Backlog
> PBIs die während eines Sprints in ein Inkrement umgesetzt werden

* Ausgehandelt von PO und Entwicklungsteam
* Fix während Sprint
* Sprint Ziel
* Arbeit ist Transparent (Scrum Board)
* Gehört dem Entwicklungsteam

+++

## Inkrement
> Summe aller umgesetzten PBIs während eines Sprints.

* Alle PBIs gemäss Definition of Done
* Nutzbar für Kunde
* Feedback im Sprint Review
* PO entscheidet über Auslieferung

+++

## Sprint Ziel

> Nutzen der durch das gelieferte Inkrement erreicht wird.

* Übergeordnete Vision des Sprints
* Unveränderlich 
* Sprint Planning 1
* Fokus Entwicklungsteam

+++

## Definition of Done

> Gemeinsames Verständnis wann PBI "fertig" ist.

* Qualitätstandard
* Nichtfunktionale Anforderungen
* Transparent
* Von allen Beteiligten verstanden
* Geprüft bei Review

---

## Scrum Prozess
![Image-Absolute](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Scrum_process-de.svg/2000px-Scrum_process-de.svg.png)

Scrum Prozess:
["Lakework"](https://de.wikipedia.org/wiki/Scrum#/media/File:Scrum_process-de.svg)|
[CC BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0)

---

Fragen?